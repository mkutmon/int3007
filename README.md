# INT3007 - Systems Biology
## Disease mechanisms practical

* **Contact**: Martina Summer-Kutmon (martina.kutmon(a)maastrichtuniversity.nl)
* **Last updated**: 27 October 2020

---

In this practical, you will learn the basic applications of **pathway** (part 1) and **network analysis** (part 2) by investigating the transcriptomics dataset for **lung cancer**.

You will learn how to 
* visualize experimental data on pathways in PathVisio [Part 1]
* perform pathway analysis using transcriptomics data in PathVisio [Part 1]
* use pathways as a resource for network analysis using Cytoscape [Part 2]
* build molecular networks using knowledge from online databases using Cytoscape [Part 2]
* extend molecular networks with regulatory and drug-target information in Cytoscape [Part 2]

---

## Instructions

* Feel free to work alone or in pairs. Step-by-step instructions are provided. 
* In between there are questions related to the methodology or biological interpretation. The aim of these questions is to make you really think about the analysis you are performing and the produced results. The answers are also available in the Word document on the student portal (Practical 1). 
* You can answer questions on the discussion board for practical 1. Instructors will montior the questions and answer them during the scheduled practical time. Don't hesitate to also consult the lecture slides or the internet to find answers yourself. 

**Let's get started:**

* Part 1: [Pathway analysis](pathways.md)
* Part 2: [Network analysis](networks.md)