# Using network analysis to study the molecular and regulatory mechanisms involved in lung cancer

In this part, we want to show you different ways of how you can build biological network relevant for a biological research topic. [Cytoscape](https://cytoscape.org/) has a lot more functionality and you are free to explore other apps in the project if you want to.  

## Setup
* Download and unzip the [network-analysis.zip](https://gitlab.com/mkutmon/int3007/raw/master/data/network-analysis.zip?inline=false) file with all required files for the practical. It contains:
  * the lung cancer dataset (lung-cancer-data.xlsx)
  * the linkset folder with the drug-target information downloaded from [CyTargetLinker](https://cytargetlinker.github.io)
* Start Cytoscape from the Start Menu
* Open the App Manager (Apps → App Manager)
* Make sure the WikiPathways, CyTargetLinker and stringApp are installed.

## Assignment 1: Drug targets in the cell cycle pathway

Using the cell cycle pathway, you will learn how pathways can be used as a resource for network biology. Besides looking at the network structure, we will also add drug-target information to see which drugs in the [DrugBank](https://www.drugbank.ca/) database are known to target genes in the cell cycle pathway.

* Go to "File → New → Session…"
* In the control panel on the left, select the WikiPathways database and search for "cell cycle" (with quotes)

![alt text](images/Net_Figure1.png "Figure 1")

* In the search result window, select "Cell cycle" pathway for Homo sapiens (human) and click "Import as Network"

![alt text](images/Net_Figure2.png "Figure 2")

* You should now see a network with 164 nodes and 223 edges (# of nodes and edges can be found in the control panel next to the network name).

>**Question 1: Network structure**<br/>Are all nodes connected or do you see subnetworks or unconnected nodes? Make a screenshot.

Now we are going to investigate which drugs are known to target the proteins in this pathway.

* Go to "Apps → CyTargetLinker → Extend network"
* Fill in the following settings:
  * Network attribute is the column in the node table that contains the gene identifier
  * Select RegINs is the linkset folder in the network-analysis folder, which contains the drug-target information.

![alt text](images/Net_Figure3.png "Figure 3")

* Now the known drugs from DrugBank have been added – information about drug name, drug category or approval status can be found in the node table.
* You can find more information about the drugs on the DrugBank website (www.drugbank.ca) to investigate if any of the drugs are used for lung cancer treatment. The identifiers and names of the drugs can be found in the node table below the network. 

>**Question 2: Network extension**<br/>How many drugs and drug-target interactions were added to the network? (check number of nodes and edges in the control panel)<br/>Which genes are targeted by many drugs?

## Assignment 2: Network analysis of protein-protein interaction network for up-regulated lung cancer genes

**Step 1: Gene selection**
* Open the dataset (lung-cancer-data.xlsx) in Excel.
* Select the significantly up-regulated genes with a log2FC > 1 → filter the table (Data → Filter) by log2FC (>1) AND the adju.P.Value (< 0.05) → 383 genes (be aware of the Dutch setting on the computers → 0,05)
* Copy the gene names (2nd column) of the 383 genes

---

**Step 2: Create protein-protein interaction network for gene selection**
* In Cytoscape, go to "File → New → Session..."
* Select the STRING protein query (drop-down box left to search field in the control panel) and paste the 383 gene names in the query field.

![alt text](images/Net_Figure4.png "Figure 4")

* After clicking the search icon, the "Resolve Ambiguous Terms" dialog will pop up. Click on "Import" without any changes. 
* Cytoscape will create a network with 335 nodes and 3,794 edges

>**Question 3: Network structure**<br/>Are all nodes connected or do you see subnetworks or unconnected nodes?

* To extract the largest connected component, go to Tools → Network Analyzer → Subnetwork creation. Select the component with the largest number of nodes (252). The new network should have 252 nodes and 3,790 edges.

---

**Step 3: Investigate the network properties of the network** 

* Go to "Tools → Network Analyzer → Network Analysis → Analyze Network..."
  * The network should be treated as an undirected network (protein-protein interactions, in general, do not have a direction)
  * <font color="red">Attention!</font> There is a problem on the computer rooms and the dialog opened by the network analyzer tool is not opened with the right dimensions - you will need to increase the window size to see the results
* This tool will calculate all network properties like degree and betweenness for you (check lecture!).

>**Question 4: Network properties**<br/>What are node degree and betweenness?<br/>Look at the "Node degree" tab in Results panel of the Network Analyzer. How does the node degree distribution look like? Is the network a scale-free network? (linked to case 1)

---

**Step 4: Create visualization to show degree and betweenness** 

* In the NetworkAnalyzer result dialog, click on “Visualize Parameters”. Select the node degree as node color gradient (Map node color to → Select “Degree”) and the node betweenness as node size.

![alt text](images/Net_Figure5.png "Figure 5")


>**Question 5: Network properties**<br/>Describe the overall network visualization. Large nodes have a high betweenness and the redder the node, the higher the degree.<br/>Can you find any hub nodes? (Hint: the node table, now has a column Degree and columns in Cytoscape tables are sortable by clicking on the header)

---

**Step 5: Find more information about major hub gene** 

*TOP2A* is a node with a high degree and betweenness in the network (Question 4). The human protein atlas is an exceptional resource to find out more information about the expression of a gene/protein in different tissues but it also provides detailed pathology analysis for relevant cancer genes.

* Go to https://www.proteinatlas.org
* Search for *TOP2A* → one of the hub genes of our network

>**Question 6: Human protein atlas**<br/>What kind of functions does TOP2A have?<br/>In which tissues it the gene/protein expressed? (Tissue Tab)<br/>Is TOP2A a prognostic marker for lung or any other cancers? (Pathology Tab)
